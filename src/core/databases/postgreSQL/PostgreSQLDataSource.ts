import { DataSource, DataSourceOptions } from "typeorm";

type IPostgreSQLDataSourceOptions = DataSourceOptions & { type: "postgres" };

export class PostgreSQLDataSource extends DataSource {
    constructor(_postgreSQLDataSourceOptions: IPostgreSQLDataSourceOptions) {
        super(_postgreSQLDataSourceOptions);
    }
}

const postgreSQLDataSource = new PostgreSQLDataSource({
    type: "postgres",
    host: process.env.APPLICATION_PGSQL_HOST,
    port: parseInt(process.env.APPLICATION_PGSQL_HOST_PORT ? process.env.APPLICATION_PGSQL_HOST_PORT : "5432"),
    username: process.env.APPLICATION_PGSQL_USER,
    password: process.env.APPLICATION_PGSQL_USER_PASSWORD,
    database: process.env.APPLICATION_PGSQL_DATABASE,
    logging: process.env.NODE_ENV == "development" ? "all" : false,
    logger: "advanced-console"
});

export default postgreSQLDataSource;