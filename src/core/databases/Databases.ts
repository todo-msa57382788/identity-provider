import IDatabase from "../../common/database/Database";

import postgreSQLDatabase from "./postgreSQL/PostgreSQLDatabase";

interface IDatabases {
    connectAll(): Promise<void>;
}

class Databases implements IDatabases {
    constructor(private _databases: IDatabase[]) {}

    public async connectAll(): Promise<void> {
        const databasesConnections = this._databases.map(database => { return database.connect(); });

        await Promise.all(databasesConnections);
    }
}

const databases = new Databases([ postgreSQLDatabase ]);

export default databases;