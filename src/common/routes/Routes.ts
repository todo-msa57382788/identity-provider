import { Router } from "express";

abstract class Routes {
    constructor(private _router: Router) {
        this.define();
    }

    get router(): Router {
        return this._router;
    }

    protected abstract define(): void;
}

export default Routes;