class EnvironmentVariables {
    constructor() {}

    public static async loadDevelopment(): Promise<void> {
        if(process.env.NODE_ENV === "development"){
            const dotenv = await import("dotenv");

            dotenv.config();
        }
    }
}

export default EnvironmentVariables;