import IDatabase from "../../../common/database/Database";

import postgreSQLDataSource, { PostgreSQLDataSource } from "./PostgreSQLDataSource";

import postgreSQLMessages from "./utils/messages/index.json";

class PostgreSQLDatabase implements IDatabase {

    constructor(private _postgreSQLDataSource: PostgreSQLDataSource){}

    public async connect(): Promise<void> {
        if(!this._postgreSQLDataSource.isInitialized)
            await this._postgreSQLDataSource.initialize();
        else
            throw new Error(postgreSQLMessages.postgresql_already_initialized);
    }
}

const postgreSQLDatabase = new PostgreSQLDatabase(postgreSQLDataSource);

export default postgreSQLDatabase;