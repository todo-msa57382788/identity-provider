import express, { Express } from "express";
import helmet from "helmet";
import cors from "cors";

import appRoutes, { IAppRoutes } from "../app-routes/AppRoutes";

import serverMessages from "./utils/messages/index.json";

interface IServer {
    initializeServer(): void;
}

class Server implements IServer {
    private static _instance: Server = new Server(express(), appRoutes);
    
    private _isServerRunning: boolean = false;

    private constructor(private _app: Express, private _appRoutes: IAppRoutes) {}

    public static get instance(): IServer {
        return Server._instance;
    }

    public initializeServer() {
        if(this._isServerRunning)
            throw new Error(serverMessages.server_already_running);

        const port = this.getServerPort();

        this._app.listen(port, () => {
            this.loadUsables();

            this._isServerRunning = true;

            console.log(`${serverMessages.server_up_running_port} ${port}`);
        });
    }

    private getServerPort(): number | never {
        const envServerPort = process.env.APPLICATION_SERVER_PORT;

        if(envServerPort && +envServerPort)
            return parseInt(envServerPort);
        else
            throw new Error(serverMessages.server_port_not_properly_defined);
    }

    private loadUsables(): void {
        this._app.use(helmet());
        this._app.use(cors());
        this._app.use(express.json());
        this._app.use(this._appRoutes.router);
    }
}

export default Server.instance;