import "reflect-metadata"

import server from "./core/server/Server";

import EnvironmentVariables from "./common/environment-variables/EnvironmentVariables";

import commonMessages from "./common/utils/messages/index.json";

class Main {
    private constructor() {}

    public static async start() {
        try{
            await EnvironmentVariables.loadDevelopment();

            const databases = (await import("./core/databases/Databases")).default;
            await databases.connectAll();

            server.initializeServer();
        }catch(error){
            console.log(`${commonMessages.something_went_wrong} ${error}`);
        }
    }
}

Main.start();