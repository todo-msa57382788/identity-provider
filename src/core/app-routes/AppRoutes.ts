import express, { Router } from "express";

import Routes from "../../common/routes/Routes";

export interface IAppRoutes {
    get router(): Router;
}

class AppRoutes implements IAppRoutes {
    constructor (private _router: Router, private _routes: Routes[]) {
        this.define();
    }

    get router(): Router {
        return this._router;
    }

    private define(): void {
        this._routes.forEach(route => this.router.use(route.router));
    }
}

const appRoutes = new AppRoutes(express.Router(), []);

export default appRoutes;